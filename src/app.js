exports.lambdaHandler = (event, context, callback) => {
	console.log(event);
    let tmp = "Hello world"
    let code = 200
    let response = {
        statusCode: code,
        headers: {
            "Access-Control-Allow-Headers" : "Content-Type",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "OPTIONS,POST,GET,PUT"
        },
        body: JSON.stringify(tmp) 
    }
    callback(null, response)
};
