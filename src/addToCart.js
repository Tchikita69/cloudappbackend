const AWS = require('aws-sdk'); 
const ddb = new AWS.DynamoDB.DocumentClient({region: 'us-east-1'}); 
exports.lambdaHandler = async (event, context, callback) => {
    await getCart(event.queryStringParameters.customerId)
    .then( async (data) => {
        console.log(data)
        var keys = Object.keys(data);

        if (!keys.length) {
            await createCart(event.queryStringParameters.customerId, event.queryStringParameters.productId)
            .then(data => {
                callback(null, {
                    statusCode: 200,
                    body: JSON.stringify([event.queryStringParameters.productId]),
                    headers: {},
                })
            })
            .catch((err) => {
                console.error(err);
            })
        } else {
            await updateCart(event.queryStringParameters.customerId, event.queryStringParameters.productId)
            .then(data => {
                callback(null, {
                    statusCode: 200,
                    body: JSON.stringify(data.Attributes.products),
                    headers: {},
                })
            })
            .catch((err) => {
                console.error(err);
            })
        }
    })
    .catch((err) => {
        console.error(err);
    })
};

function updateCart(userId, productId) {
    const params = {
        TableName: "gregoinfoCarts",
        Key: {
             "id": userId
        },
        UpdateExpression: "SET #ri = list_append(if_not_exists(#ri, :empty_list), :vals)",
        ExpressionAttributeNames: {
            "#ri": "products"
        },
        ExpressionAttributeValues: {
            ":vals": [productId],
            ":empty_list": {"L": []}
        },
        ReturnValues: 'ALL_NEW'
    }
    return ddb.update(params).promise();
}

function createCart(userId, productId) {
    const params = {
        TableName : 'gregoinfoCarts',
        Item: {
            id: userId,
            products: [productId]
        }
    }
    return ddb.put(params).promise();
}

function getCart(userId) {
    const params = {
        TableName: 'gregoinfoCarts',
        Key: {
            'id': userId
        }
    }
    return ddb.get(params).promise();
}