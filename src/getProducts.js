const AWS = require('aws-sdk'); 
const ddb = new AWS.DynamoDB.DocumentClient({region: 'us-east-1'}); 
exports.lambdaHandler = async (event, context, callback) => {
    await readMessage()
    .then(data => {
        data.Items.forEach(function(item) {
            console.log(item.message)
        });
        callback(null, {
            statusCode: 200,
            body: JSON.stringify(data.Items),
            headers: {},
        })
    })
    .catch((err) => {
        console.error(err);
    })
};

function readMessage() {
    const params = {
        TableName: 'gregoinfoProducts2',
        Select: "ALL_ATTRIBUTES"
    }
    return ddb.scan(params).promise();
}