// const AWS = require("aws-sdk");
// var client = new AWS.DynamoDB.DocumentClient();
// var s3 = new AWS.S3();

// exports.lambdaHandler = (event, context, callback) => {
//     console.log(event)
//     // decode base64 jsonBodie
//     let buff = Buffer.from(event.body, 'base64');
//     let bodie = buff.toString('ascii');
//     let jsonBodie = JSON.parse(bodie)

//     let decodedImage = Buffer.from(jsonBodie.image, 'base64');
//     console.log(decodedImage);
//     var filepath = "productImages/" + jsonBodie.name + ".jpg";
//     var s3params = {
//         "Body": decodedImage,
//         "Bucket": "grego-info-pictures",
//         "Key": filepath,
//         "ACL": "public-read-write"
//     };
//     s3.upload(s3params, function(err, data){
//        if(err) {
//            console.log("oh no:" + err);
//        } else {
//             console.log("oh yes:" + data);
//         }
//     });
//     console.log('salut');

//     console.log("Name:" + jsonBodie.name + "VendorId:" + jsonBodie.vendorId + "Keywords:" + jsonBodie.keywords + "Price: " + jsonBodie.price)
//     var params = {
//         TableName : 'gregoinfoProducts2',
//         Item: {
//             name: jsonBodie.name,
//             vendorId: jsonBodie.vendorId,
//             keywords: jsonBodie.keywords,
//             price: jsonBodie.price,
//             imageURL: "https://grego-info-pictures.s3.amazonaws.com/" + filepath
//         }
//     };
//     client.put(params, function(err, data) {
//         if (err) {
//             console.log(err);
//             callback(err)
//         } else {
//             callback(null, {
//                 statusCode: 200,
//                 headers: {
//                 },
//                 body: data
//             })
//         }
//     });
// };

const AWS = require('aws-sdk'); 
const ddb = new AWS.DynamoDB.DocumentClient({region: 'us-east-1'}); 
const s3 = new AWS.S3();

exports.lambdaHandler = async (event, context, callback) => {
    console.log(event)
    // decode base64 jsonBodie
    let buff = Buffer.from(event.body, 'base64');
    let bodie = buff.toString('ascii');
    let jsonBodie = JSON.parse(bodie)
    let filepath = "productImages/" + jsonBodie.name.split(' ').join('_') + ".jpg";

    console.log(jsonBodie)
    await uploadToS3(jsonBodie.image, filepath)
    .then( async (data) => {
        await addProductToTable(jsonBodie, filepath)
        .then(data => {
            callback(null, {
                statusCode: 200,
                body: JSON.stringify(data),
                headers: {},
            })
        })
        .catch(err => {
            console.error(err)
        })
    })
    .catch((err) => {
        console.error(err);
    })
};

function uploadToS3(image, filepath) {
    let decodedImage = Buffer.from(image, 'base64');

    const params = {
        "Body": decodedImage,
        "Bucket": "grego-info-pictures",
        "Key": filepath,
        "ACL": "public-read-write"
    }
    return s3.upload(params).promise();
}

function addProductToTable(jsonBodie, filepath) {
    const params = {
        TableName : 'gregoinfoProducts2',
        Item: {
            name: jsonBodie.name,
            vendor: jsonBodie.vendorId,
            keywords: jsonBodie.keywords,
            price: jsonBodie.price,
            imageURL: "https://grego-info-pictures.s3.amazonaws.com/" + filepath
        }
    };
    return ddb.put(params).promise();
}
