const AWS = require('aws-sdk'); 
const ddb = new AWS.DynamoDB.DocumentClient({region: 'us-east-1'}); 
exports.lambdaHandler = async (event, context, callback) => {
    await getCart(event.queryStringParameters.customerId)
    .then(data => {
        console.log(data)
        callback(null, {
            statusCode: 200,
            body: JSON.stringify(data),
            headers: {},
        })
    })
    .catch((err) => {
        console.error(err);
    })
};

function getCart(userId) {
    const params = {
        TableName: 'gregoinfoCarts',
        Key: {
            'id': userId
        }
    }
    return ddb.get(params).promise();
}