AWSTemplateFormatVersion: '2010-09-09'
Transform: 'AWS::Serverless-2016-10-31'
Description: SAM template for gregoinfo backend, that handles DynamoDB, Lambda and API Gateway, and Cognito Auth.

Parameters:
  UserPoolId:
    Type: String
    Description: User poolID for Cognito provider
  UserAudience:
    Type: String
    Description: Client id for user pool
  VendorPoolId:
    Type: String
    Description: Vendor poolID for Cognito provider
  VendorAudience:
    Type: String
    Description: Client id for user pool

Globals:
  Function:
    Timeout: 5
    Runtime: nodejs12.x

Resources:
  # COGNITO USER POOL
  UserPool:
    Type: AWS::Cognito::UserPool 
    Properties: 
      UserPoolName: Sometest-UserPool 
      Policies: 
        PasswordPolicy: 
          MinimumLength: 8
      AutoVerifiedAttributes:
        - email
      UsernameAttributes: 
        - email
      Schema: 
        - AttributeDataType: String 
          Name: email 
          Required: false

  VendorPool:
    Type: AWS::Cognito::UserPool 
    Properties: 
      UserPoolName: Sometest-VendorPool 
      Policies: 
        PasswordPolicy: 
          MinimumLength: 8
      AutoVerifiedAttributes:
        - email
      UsernameAttributes: 
        - email
      Schema: 
        - AttributeDataType: String 
          Name: email 
          Required: false
        - AttributeDataType: String 
          Name: name
          Required: true
        - AttributeDataType: String 
          Name: family_name
          Required: true
        - AttributeDataType: String 
          Name: phone_number
          Required: true

  # IAM ROLE
  LambdaRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - 
            Effect: "Allow"
            Principal:
              Service:
              - "lambda.amazonaws.com"
            Action:
              - 'sts:AssumeRole'
      Policies:
        - PolicyName: "gregoinfo_access"
          PolicyDocument:
            Version: "2012-10-17"
            Statement:
              - 
                Effect: "Allow"
                Action: "*"
                Resource: "*"
  # API
  HttpApi:
    Type: AWS::Serverless::HttpApi
    Properties:
      Auth:
        Authorizers:
          GeneralAuth:
            IdentitySource: "$request.header.Authorization"
            JwtConfiguration:
              issuer: !Sub https://cognito-idp.${AWS::Region}.amazonaws.com/${UserPoolId}
              audience:
                - !Ref UserAudience
          VendorAuth:
            IdentitySource: "$request.header.Authorization"
            JwtConfiguration:
              issuer: !Sub https://cognito-idp.${AWS::Region}.amazonaws.com/${VendorPoolId}
              audience:
                - !Ref VendorAudience
      CorsConfiguration:
        AllowMethods:
          - GET
          - POST
          - DELETE
        AllowOrigins:
          - '*'
        AllowHeaders:
          - 'authorization'
  
  # DYNAMODB TABLES
  gregoinfoProductsTable:
    Type: 'AWS::DynamoDB::Table'
    Properties:
      TableName: gregoinfoProducts2
      AttributeDefinitions:
        - AttributeName: name
          AttributeType: S
      KeySchema:
        - AttributeName: name
          KeyType: HASH
      BillingMode: PAY_PER_REQUEST

  gregoinfoCarts:
    Type: 'AWS::DynamoDB::Table'
    Properties:
      TableName: gregoinfoCarts
      AttributeDefinitions:
        - AttributeName: id
          AttributeType: S
      KeySchema:
        - AttributeName: id
          KeyType: HASH
      BillingMode: PAY_PER_REQUEST

  # LAMBDA FUNCTIONS
  # LAMBDA FUNCTIONS - Public
  getProducts:
    Type: AWS::Serverless::Function
    Properties:
      CodeUri: src/
      Handler: getProducts.lambdaHandler
      Description: Function that returns all products available for gregoinfo
      Role: !GetAtt LambdaRole.Arn
      # Policies:
      #   - DynamoDBCrudPolicy:
      #       TableName: gregoinfoProducts2
      Events:
        GetProducts:
          Type: HttpApi
          Properties:
            Path: /products
            Method: get
            ApiId: !Ref HttpApi

  # LAMBDA FUNCTIONS - Auth
  getCart:
    Type: AWS::Serverless::Function
    Properties:
      CodeUri: src/
      Handler: getCart.lambdaHandler
      Description: Returns the cart for a user
      Role: !GetAtt LambdaRole.Arn
      # Policies:
      #   - DynamoDBCrudPolicy:
      #       TableName: gregoinfoCarts
      Events:
        GetProducts:
          Type: HttpApi
          Properties:
            Auth:
              Authorizer: GeneralAuth
            Path: /cart
            Method: get
            ApiId: !Ref HttpApi
  addToCart:
    Type: AWS::Serverless::Function
    Properties:
      CodeUri: src/
      Handler: addToCart.lambdaHandler
      Description: Adds a product to user card
      Role: !GetAtt LambdaRole.Arn
      # Policies:
      #   - DynamoDBCrudPolicy:
      #       TableName: gregoinfoCarts
      Events:
        GetProducts:
          Type: HttpApi
          Properties:
            Auth:
              Authorizer: GeneralAuth
            Path: /cart/product
            Method: post
            ApiId: !Ref HttpApi
  postProduct:
    Type: AWS::Serverless::Function
    Properties:
      CodeUri: src/
      Handler: postProduct.lambdaHandler
      Description: Create a new product as a vendor
      Role: !GetAtt LambdaRole.Arn
      # Policies:
      #   - DynamoDBCrudPolicy:
      #       TableName: gregoinfoProducts2
      Events:
        PostProduct:
          Type: HttpApi
          Properties:
            Auth:
              Authorizer: VendorAuth
            Path: /vendor/product
            Method: POST
            ApiId: !Ref HttpApi